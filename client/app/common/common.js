import angular from 'angular';
import Navbar from './navbar/navbar';
import Todo from './todo/todo';


let commonModule = angular.module('app.common', [
  Navbar,
  Todo  
])
  
.name;

export default commonModule;
