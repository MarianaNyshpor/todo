class TodoService {
  constructor($http, /*$httpBackend*/) {
    'ngInject';
    this.$http = $http;
    /*this.$httpBackend = $httpBackend;*/
  }
  
  getTodos() {
    let todos = [
      {id: 5, title: "clean the house", selected: false},
      {id: 4, title: "run", selected: false},
      {id: 3, title: "sweem", selected: false},
      {id: 2, title: "sleep", selected: false},
      {id: 1, title: "dance", selected: false}
    ];
    return todos;
    //$httpBackend.whenGET('/tasks').respond(data);*/
    //this.$http.get('https://jsonplaceholder.typicode.com/todos')
    // .then(response => response.data)
  }

}


export default TodoService;