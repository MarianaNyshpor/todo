
class TodoController {
    
    constructor(TodoService) {
     'ngInject';
     this.TodoService = TodoService;
     this.name = 'todo';
    }
    $onInit() {
      this.newTodo = {
        title: '',
        selected: false
      };
     
      this.todos = [];
      this.todos = this.TodoService.getTodos();
      // this.todos = this.TodoService.getTodos().then(response => this.todos = response); 
      }

    addTodo({ todo }) {
      if (!todo) return;
        this.todos.unshift(todo);
        this.newTodo = {
          title: '',
          selected: false
      };
    }

    filterTodo({ todo }) {
      if (!todo) return;
        this.todos = this.todos.filter(elem => elem.title === todo.title);
        this.newTodo = {
          title: '',
          selected: false
        };
    }
  }
export default TodoController;