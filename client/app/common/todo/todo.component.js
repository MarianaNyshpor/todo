import template from './todo.html';
import controller from './todo.controller';
import './todo.scss';

let todoComponent = {
  bindings: {},
  template,
  controller
  
};

export default todoComponent;