import angular from 'angular';
import uiRouter from 'angular-ui-router';
import todoComponent from './todo.component';
import TodoForm from './todo-form/todo-form';
import TodoList from './todo-list/todo-list';
import TodoFilter from './todo-filter/todo-filter';
//import ngMockE2E from 'angular-mocks';
import TodoService from './todo.service';


let todoModule = angular
  .module('todo', [
  	uiRouter,
  	TodoForm,
  	TodoList,
  	TodoFilter,
    //'ngMockE2E'
    
  	
  ])
  .component('todo', todoComponent)
  .service('TodoService', TodoService)
  .name;

export default todoModule;