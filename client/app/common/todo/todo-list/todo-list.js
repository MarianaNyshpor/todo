import angular from 'angular';
import todoListComponent from './todo-list.component';

let todoListModule = angular
  .module('todo.list', [
  	])
  .component('todoList', todoListComponent)
  .value('EventEmitter', payload => ({ $event: payload }))
  .name;

export default todoListModule;