import template from './todo-list.html';
import './todo-list.scss';

let todoListComponent = {
  bindings: {
    todo: '<',
    todos: '<',
  },
  template,

  controller: class ListComponent {
    constructor(EventEmitter) {
      'ngInject';
       this.EventEmitter = EventEmitter;
       this.name = 'todoList';
    }

    $onInit() {
      this.counter = 0;
    }

    $onChanges(changes) {
      if (changes.todo) {
        this.todo = Object.assign({}, this.todo);
      } else if (changes.todos){
        this.todos = Object.assign({}, this.todos);
      }
    }
     
    onRemove(index) {
      this.todos.splice(index, 1);
    }

    onSelect(index) {
      this.todos[index].selected = !this.todos[index].selected;
    }

    onCount() {
      const number = this.todos.filter(elem => elem.selected === false);
      this.counter = number.length;
    };

    onArchive() {
      this.todos = this.todos.filter(elem => elem.selected === false);
    };
  }
};

export default todoListComponent;