import template from './todo-filter.html';
import './todo-filter.scss';

let todoFilterComponent = {
  bindings: {
    todo: '<',
    onFilterTodo: '&'
  },
  template,

  controller: class FilterComponent {
    constructor(EventEmitter) {
      'ngInject';
      this.EventEmitter = EventEmitter;
      this.name = 'todoFilter';
    }
    $onChanges(changes) {
      if (changes.todo) 
        this.todo = Object.assign({}, this.todo);
    }

    onSubmit() {
      if (!this.todo.title) return;
        this.onFilterTodo(
          this.EventEmitter({
            todo: this.todo
          })
        );
    }
  }
};

export default todoFilterComponent;