import angular from 'angular';
import todoFilterComponent from './todo-filter.component';


let todoFilterModule = angular
  .module('todo.filter', [])
  
  .component('todoFilter', todoFilterComponent)
  
  .value('EventEmitter', payload => ({ $event: payload }))
  .name;

export default todoFilterModule;