import angular from 'angular';
import todoFormComponent from './todo-form.component';


let todoFormModule = angular
  .module('todo.form', [])
  .component('todoForm', todoFormComponent)
  .value('EventEmitter', payload => ({ $event: payload }))
  .name;

export default todoFormModule;