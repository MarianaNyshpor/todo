import template from './todo-form.html';
import './todo-form.scss';

let todoFormComponent = {
  bindings: {
    todo: '<',
    onAddTodo: '&'
  },
  template,

  controller: class FormComponent {
    constructor(EventEmitter) {
      'ngInject';
       this.EventEmitter = EventEmitter;
       this.name = 'todoForm';
    }

    $onChanges(changes) {
      if (changes.todo) {
        this.todo = Object.assign({}, this.todo);
      }
    }

    onSubmit() {
      if (!this.todo.title) return;
        this.onAddTodo(
          this.EventEmitter({
            todo: this.todo
          })
        );
    }
  }
};

export default todoFormComponent;